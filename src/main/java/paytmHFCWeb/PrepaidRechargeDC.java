package paytmHFCWeb;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import credsHelper.Creds;

public class PrepaidRechargeDC {
	@BeforeTest
	public void setUp() throws InterruptedException {
		System.out.println("Running Prepaid Recharge with Debit Card");
		Helper.startTime();
		Helper.setUpSSO();
	}
	
	@Test(priority = 1)
	public void RechargeProcess() throws Exception{
		Helper.noteTime("//*[@id='nav-xshop']/a[3]", "InitialPage");
		Helper.startTime();

		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='dashboard-container']/div/div[2]/div/div[4]/a")))
				.click();
		
		Helper.noteTime("//*[@id='mobileNumberTextInputId']", "RechargePage");
		Helper.startTime();

		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='mobileNumberTextInputId']")))
					.sendKeys(Creds.decrypt(Creds.RECHARGE_MOBILE_NUMBER));
		
		Thread.sleep(500);
		
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='amountTextInputId']")))
					.sendKeys(Creds.RECHARGE_AMOUNT);
		
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='buyButtonNative']")))
				.click();
		
		Helper.noteTime("//*[@id='ap_email']", "LoginPageUserName");
		Helper.startTime();

		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='ap_email']")))
					.sendKeys(Creds.decrypt(Creds.AMAZON_LOGIN_USERNAME));
		
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='continue']")))
					.click();
		
		Helper.noteTime("//*[@id='ap_email']", "LoginPagePassword");
		Helper.startTime();

		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='ap_password']")))
					.sendKeys(Creds.decrypt(Creds.AMAZON_LOGIN_PASSWORD));
				
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='signInSubmit']")))
					.click();
	}
	
	@Test (priority=2)
	public void paymentSelection() throws Exception {
		Helper.noteTime("//*[@id='payment-change-link']", "PlaceOrderPage");
		
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='payment-change-link']"))).click();
		
		Helper.noteTime("//*[@id='new-vdc']/div[1]/span[1]/span/span/button", "PaymentPage");
		
		Helper.js.executeScript("scroll(0, 500);");
		//Select Debit Card Option
		System.out.println("Select DC Option");
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='new-vdc']/div[1]/span[1]/span/span/button"))).click();
		
//		Helper.wait.until(ExpectedConditions.elementToBeClickable(
//				By.xpath("//*[@data-value='OtherBanks']"))).click();
		//*[@id="1_dropdown_combobox"]/li[2]/a
		Helper.wait.until(ExpectedConditions.elementToBeClickable(
			By.xpath("//*[@id='1_dropdown_combobox']/li[2]/a"))).click();
		
		System.out.println("Enter Debit Card Name");
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='vdcName']")))
					.sendKeys(Creds.decrypt(Creds.ICICI_DEBIT_CARD_NAME));
		
		System.out.println("Enter Debit Card Number");					
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath(("//*[@id='newVerifiedDebitCardNumber']"))))
				.sendKeys(Creds.decrypt(Creds.ICICI_DEBIT_CARD));
		
		Thread.sleep(200);
		//select expiry month 
		Helper.wait.until(ExpectedConditions.elementToBeClickable(
				By.xpath("//*[@id='expirationInput']/div/span[1]/span/span/button"))).click();
		Helper.wait.until(ExpectedConditions.elementToBeClickable(
				By.xpath("//*[@data-value='"+Creds.ICICI_DEBIT_CARD_EXPIRY_MONTH_AMAZON_WEB+"']"))).click();
		
		Thread.sleep(200);
		//select expiry year
		Helper.wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='expirationInput']/div/span[2]/span/span/button"))).click();
		
		Helper.wait.until(ExpectedConditions.elementToBeClickable(
				By.xpath("//*[@data-value='"+Creds.ICICI_DEBIT_CARD_EXPIRY_YEAR_AMAZON_WEB+"']"))).click();
		
		//Enter CVV
		System.out.println("Enter CVV number");
		Helper.wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='vdcCVVNum']"))).
																		sendKeys(Creds.decrypt(Creds.ICICI_DEBIT_CARD_CVV));
		//Click Add your card
		Helper.wait.until(ExpectedConditions.elementToBeClickable(By.name("addVDC"))).click();
		
		//Scroll down and click on continue button
		while(Helper.driver.findElement(By.id("continue-bottom")).isDisplayed()){
			if(!Helper.driver.findElement(By.id("loading-spinner-blocker-doc")).isDisplayed()){
				Thread.sleep(1000);
				Helper.js.executeScript("scroll(0, 2000);");
				break;
			}
		}

		Helper.wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='continue-bottom']"))).click();
		
		System.out.println("Click Place your order btn");
		Helper.wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='order-summary-box']/div[1]/div/div[1]/div/span/span/input"))).click();
	}
	
	@Test (priority=3)
	public void payment() throws Exception {
		System.out.println("Enter Debit Card Password");					
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='enterPASS']")))
				.sendKeys(Creds.decrypt(Creds.ICICI_DEBIT_CARD_PASSWORD));
		
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='sendotp']"))).click();
		
		Helper.waitForSuccess();
		Helper.noteTime("", "BankPayment");
	}	
	
	@AfterTest
	public void tearDown() throws InterruptedException{
		String harFileName = this.getClass().getSimpleName();
		Helper.generateHarFile(harFileName);
		Helper.endScript();
	}
}