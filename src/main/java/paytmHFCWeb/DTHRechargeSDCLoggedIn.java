package paytmHFCWeb;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import credsHelper.Creds;

public class DTHRechargeSDCLoggedIn {
	@BeforeTest
	public void setUp() throws InterruptedException {
		System.out.println("Running Prepaid Recharge with Debit Card");
		Helper.startTime();
		Helper.setUpNonSSO();
	}
	
	@Test(priority = 1)
	public void RechargeProcess() throws Exception{
		Helper.noteTime("//*[@id='nav-xshop']/a[3]", "InitialPage");
		Helper.startTime();

		//Select DTH recharge
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='dashboard-container']/div/div[2]/div/div[5]/a")))
							.click();
		
		Helper.noteTime("//*[@id='nav-xshop']/a[3]", "DTHOPSelectionPage");
		Helper.startTime();
				
		//Select DTH operator
		Helper.wait.until(
				ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='a-page']/div[3]/div/div/div[2]/div[3]/a/div[1]/img")))
				.click();
		
		Helper.noteTime("//*[@id='nav-xshop']/a[3]", "DTHDetailPage");
		Helper.startTime();
		
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='auth_1']")))
					.sendKeys(Creds.decrypt(Creds.DTH_NUMBER));
		
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='dth-section']/div[3]/div[2]/div/div/input")))
					.sendKeys(Creds.DTH_RECHARGE_AMOUNT);
		
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='paymentBtnId']/span/input")))
				.click();
		
		//If password asking again
		if(Helper.driver.getCurrentUrl().indexOf("https://www.amazon.in/ap/signin") >= 0){
			Helper.wait.until(
					ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='ap_password']")))
					.sendKeys(Creds.decrypt(Creds.AMAZON_LOGIN_PASSWORD));	
			
			Helper.wait.until(
					ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='signInSubmit']")))
					.click();
		}
	}
	
	@Test (priority=2)
	public void paymentSelection() throws Exception {
		Helper.noteTime("//*[@id='pm_new_verified_credit_card']", "PlaceOrderPage");

		if(Helper.driver.findElements(By.id("payment-change-link")).size() != 0){
			Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='payment-change-link']"))).click();
		}else{
			Helper.wait.until(
					ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='pm_gc_checkbox']"))).click();
		}
		
		Helper.noteTime("//*[@id='new-vdc']/div[1]/span[1]/span/span/button", "PaymentPage");

		//Select Saved Debit Card Option
		System.out.println("Select DC Option");
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='pm_0']"))).click();
		
		//Enter CVV
		System.out.println("Enter CVV number");
		Helper.wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='existingCvvNum']"))).
																		sendKeys(Creds.decrypt(Creds.ICICI_DEBIT_CARD_CVV));
		
		Helper.wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='continue-top']"))).click();
		
		System.out.println("Click Place your order btn");
		Helper.wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='order-summary-box']/div[1]/div/div[1]/div/span/span/input"))).click();
	}
	
	@Test (priority=3)
	public void payment() throws Exception {
		System.out.println("Enter Debit Card Password");					
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='enterPASS']")))
				.sendKeys(Creds.decrypt(Creds.ICICI_DEBIT_CARD_PASSWORD));
		
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='sendotp']"))).click();
		
		Helper.waitForSuccess();
		Helper.noteTime("", "BankPayment");
	}	
	
	@AfterTest
	public void tearDown() throws InterruptedException{
		String harFileName = this.getClass().getSimpleName();
		Helper.generateHarFile(harFileName);
		Helper.endScript();
	}
}