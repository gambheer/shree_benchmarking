package paytmHFCWeb;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import credsHelper.Creds;

public class PrepaidRechargePaytmWallet {
	@BeforeTest
	public void setUp() throws InterruptedException {
		System.out.println("Running Prepaid Recharge with Paytm Wallet balance");
		Helper.startTime();
		Helper.setUpSSO();
	}
	
	@Test(priority = 1)
	public void RechargeProcess() throws Exception{
		Helper.noteTime("//*[@id='app']/div/div[2]/div[1]/div[2]/span[1]/span", "Initial Page");
		Helper.startTime();
		
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='app']/div/div[3]/div/div[2]/div[1]/div[1]/div/div[1]/div[2]/ul/li[1]/div/div/input")))
											.sendKeys(Creds.decrypt(Creds.RECHARGE_MOBILE_NUMBER));
		Thread.sleep(1000);

		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='app']/div/div[3]/div/div[2]/div[1]/div[1]/div/div[1]/div[2]/ul/li[4]/div/div/input")))
											.sendKeys(Creds.RECHARGE_AMOUNT);
		
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='app']/div/div[3]/div/div[2]/div[1]/div[1]/div/div[1]/div[3]/div[2]/button")))
											.click();

		Helper.noteTime("//*[@id='app']/div/div[4]/div[2]/div/div[1]/div/ul/div[2]/li[2]/button", "RechargePage");
		Helper.startTime();

		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='app']/div/div[4]/div[2]/div/div[1]/div/ul/div[2]/li[2]/button")))
					.click();
	}
	
	@Test (priority=2)
	public void login() throws Exception {
		Thread.sleep(1000);
		Helper.login();
	}
	
	@Test (priority=2)
	public void payment() throws InterruptedException {
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='paymentBox']/div[2]/div[7]/form/div/button")))
					.click();
		
		Helper.waitForSuccess();
		Helper.noteTime("", "ThankYouPage");		
	}
	
	@AfterTest
	public void tearDown() throws InterruptedException{
		String harFileName = this.getClass().getSimpleName();
		Helper.generateHarFile(harFileName);
		Helper.endScript();
	}
}