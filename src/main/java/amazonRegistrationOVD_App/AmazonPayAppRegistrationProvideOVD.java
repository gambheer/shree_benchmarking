package amazonRegistrationOVD_App;

import java.net.MalformedURLException;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class AmazonPayAppRegistrationProvideOVD {
	@BeforeTest
	public void setUp() throws InterruptedException, MalformedURLException {
		System.out.println("Running Amazon Pay Registration ProvideOVD");
		Helper.setUpSSO();
	}
	
	@Test(priority = 1)
	public void RegistrationProcess() throws Exception{
		//Start Humburger Widget
		System.out.println("Via Humburger Menu");
		Helper.wait.until(
				ExpectedConditions.presenceOfElementLocated(By.id("in.amazon.mShop.android.shopping:id/action_bar_burger_icon"))).click();
		
		Thread.sleep(2000);
		
		Helper.startTime();

		System.out.println("Click Amazon Pay");
		Helper.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.LinearLayout[@index='7']"))).click();
		//End Humburger Menu
		
		/*
		//Start Halo Widget
		System.out.println("Via Halo Widget");
		Helper.wait.until(
				ExpectedConditions.presenceOfElementLocated(By.xpath("//android.view.View[@content-desc='Amazon Pay']"))).click();
		
		Thread.sleep(3000);
		//End Halo Widget
		*/
		
		Helper.noteTime("//android.view.View[@content-desc='Add Money']", "InitialPage");
		
		Helper.startTime();
		Helper.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.view.View[@content-desc='Add Money']"))).click();
		
		Helper.noteTime("//*[@resource-id='ap_email_login']", "LoginPage");

		Helper.wait.until(
				ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@resource-id='ap_email_login']"))).sendKeys("8432211224");
		
		Helper.startTime();
		Helper.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@resource-id='continue']"))).click();
		
		Helper.noteTime("//*[@resource-id='ap_password']", "PasswordPage");
		Helper.wait.until(
				ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@resource-id='ap_password']"))).sendKeys("Abcd@123");
		
		Helper.startTime();
		Helper.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@resource-id='signInSubmit']"))).click();
	}
	
	@Test (priority=2)
	public void completeRegistration() throws Exception {
		Helper.noteTime("//*[@resource-id='ovdValue']", "OvdPage");
		
		Thread.sleep(2000);

		Helper.wait.until(
				ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@content-desc='Provide other ID']"))).click();
		
		Helper.wait.until(
				ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.Spinner[@resource-id='ovdTypeDropdown']"))).click();

		Thread.sleep(2000);
		
		Helper.wait.until(
				ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@content-desc='Pan Card']"))).click();
		
		Helper.wait.until(
				ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@resource-id='ovdValue']"))).sendKeys("7878787878");
		
		Helper.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@content-desc='Continue']"))).click();
		
		Thread.sleep(5000);
		Helper.noteTime("", "EndPage");
	}
	
	@AfterTest
	public void tearDown() throws InterruptedException{
		String harFileName = this.getClass().getSimpleName();
		Helper.generateHarFile(harFileName);
		Helper.endScript();
	}
}