package amazonRegistrationOVD_App;

import java.net.MalformedURLException;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class AmazonPayAppRegistrationOVDMobileNotVerified {
	@BeforeTest
	public void setUp() throws InterruptedException, MalformedURLException {
		System.out.println("Running Amazon Pay Registration OVD MobileNotVerified");
		Helper.setUpSSO();
	}
	
	@Test(priority = 1)
	public void RegistrationProcess() throws Exception{
		//Start Humburger Widget
		System.out.println("Via Humburger Menu");
		Helper.wait.until(
				ExpectedConditions.presenceOfElementLocated(By.id("in.amazon.mShop.android.shopping:id/action_bar_burger_icon"))).click();
		
		Thread.sleep(2000);
		
		Helper.startTime();

		System.out.println("Click Amazon Pay");
		Helper.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.LinearLayout[@index='7']"))).click();
		//End Humburger Menu
		
		/*
		//Start Halo Widget
		System.out.println("Via Halo Widget");
		Helper.wait.until(
				ExpectedConditions.presenceOfElementLocated(By.xpath("//android.view.View[@content-desc='Amazon Pay']"))).click();
		
		Thread.sleep(3000);
		//End Halo Widget
		*/
		
		Helper.noteTime("//android.view.View[@content-desc='Add Money']", "InitialPage");
		
		Helper.startTime();
		Helper.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.view.View[@content-desc='Add Money']"))).click();
		
		Helper.noteTime("//*[@resource-id='ap_email_login']", "LoginPage");

		Helper.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.RadioButton[@index='0']"))).click();
		//content-desc='Create an account. New to Amazon? '
		
		Helper.wait.until(
				ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@resource-id='ap_customer_name']"))).sendKeys("Auriga");
		
		Helper.wait.until(
				ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@resource-id='ap_phone_number']"))).sendKeys("7014629596");
		
		Helper.wait.until(
				ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@resource-id='ap_password']"))).sendKeys("Abcd@123");
		
		Helper.startTime();
		Helper.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@resource-id='continue']"))).click();
	}
	
	@Test (priority=2)
	public void completeRegistration() throws Exception {
		Helper.noteTime("//*[@resource-id='auth-pv-enter-code']", "MobileVerificationPage");

		//Wait for OTP and enter manually
		Thread.sleep(5000);
		
		Helper.startTime();
		Helper.wait.until(
				ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@resource-id='auth-verify-button']"))).click();
		
		Helper.noteTime("", "EndPage");
	}
	
	@AfterTest
	public void tearDown() throws InterruptedException{
		String harFileName = this.getClass().getSimpleName();
		Helper.generateHarFile(harFileName);
		Helper.endScript();
	}
}