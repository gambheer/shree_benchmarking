package amazonRegistrationOVD_AWeb;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class AmazonPayA_WebRegistrationOVDMobileNotVerified {
	@BeforeTest
	public void setUp() throws InterruptedException {
		System.out.println("Running Amazon Pay Registration OVD");
		Helper.setUpSSO();
	}
	
	@Test(priority = 1)
	public void RegistrationProcess() throws Exception{
		Helper.noteTime("//*[@id='register_accordion_header']", "RegistrationPage");
		
		//Click on Create Account
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='register_accordion_header']")))
							.click();
		
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='ap_customer_name']"))).sendKeys("Auriga");
		
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='ap_phone_number']")))
							.sendKeys("7014629596");
		
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='ap_password']")))
							.sendKeys("Abcd@123");
		
		Helper.startTime();
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='continue']")))
							.click();
		
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='auth-verify-button']"))).click();
		
		if(Helper.verifyOTP()){
			Helper.startTime();
			Helper.wait.until(
					ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='auth-verify-button']"))).click();
		}
		
		Helper.noteTime("//*[@id='auth-pv-enter-code']", "MobileVerificationPage");
	}
	
	@Test (priority=2)
	public void completeRegistration() throws Exception {
		Helper.noteTime("//*[@id='ovdValue']", "OvdPage");

		Helper.js.executeScript("scroll(0, 1000);");
				
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='ovdValue']"))).sendKeys("787878787878");
		
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='insva-registration-button']/span/input")))
					.click();
		
		Helper.noteTime("", "EndPage");		
	}
	
	@AfterTest
	public void tearDown() throws InterruptedException{
		String harFileName = this.getClass().getSimpleName();
		Helper.generateHarFile(harFileName);
		Helper.endScript();
	}
}