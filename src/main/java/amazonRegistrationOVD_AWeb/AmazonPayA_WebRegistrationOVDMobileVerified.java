package amazonRegistrationOVD_AWeb;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class AmazonPayA_WebRegistrationOVDMobileVerified {
	@BeforeTest
	public void setUp() throws InterruptedException {
		System.out.println("Running Amazon Pay Registration OVD");
		Helper.setUpSSO();
	}
	
	@Test(priority = 1)
	public void RegistrationProcess() throws Exception{
		Helper.noteTime("//*[@id='ap_email_login']", "LoginPage");
		
		Helper.wait.until(
				ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='ap_email_login']"))).sendKeys("8432211224");
		
		Helper.startTime();
		Helper.driver.findElement(By.xpath("//*[@id='ap_login_form']")).submit();
		
		Helper.noteTime("//*[@id='ap_password']", "PasswordPage");
		
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='ap_password']"))).sendKeys("Abcd@123");
		
		Helper.startTime();
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='signInSubmit']"))).click();
	}
	
	@Test (priority=2)
	public void completeRegistration() throws Exception {
		Helper.noteTime("//*[@id='ovdValue']", "OvdPage");

		Helper.js.executeScript("scroll(0, 1000);");
				
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='ovdValue']"))).sendKeys("787878787878");
		
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='insva-registration-button']/span/input")))
					.click();
		
		Helper.noteTime("", "EndPage");		
	}
	
	@AfterTest
	public void tearDown() throws InterruptedException{
		String harFileName = this.getClass().getSimpleName();
		Helper.generateHarFile(harFileName);
		Helper.endScript();
	}
}