package amazonRegistrationOVD_AWeb;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Timestamp;
import java.time.Duration;
import java.time.Instant;
import java.util.NoSuchElementException;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Platform;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import net.lightbody.bmp.BrowserMobProxy;
import net.lightbody.bmp.BrowserMobProxyServer;
import net.lightbody.bmp.client.ClientUtil;
import net.lightbody.bmp.core.har.Har;
import net.lightbody.bmp.proxy.CaptureType;
import io.appium.java_client.remote.MobileCapabilityType;
import credsHelper.Creds;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class Helper{
	public static String URL = "https://www.amazon.in/gp/sva/dashboard";

	public static WebDriverWait wait;
	public static WebDriver driver;
	public static BrowserMobProxy proxy;
	public static ExpectedConditions expectedCondition;
	
	public static String driverPath = System.getProperty("user.dir") + "\\resources\\";
	public static String harFilePath = System.getProperty("user.dir") + "\\resources\\harfiles\\";
	public static String logFilePath = System.getProperty("user.dir") + "\\resources\\logfiles\\";
	
	static Proxy seleniumProxy;	
	
	public static JavascriptExecutor js;
	
	public static Instant start ;
	public static Instant end ;
	public static Duration timeElapsed;
	
	public static int total=0;
	
	public static String logJson = "";
	public static String seperator = ",";

	public static void setUpSSO() throws InterruptedException{
		//Start Proxy
		proxy = new BrowserMobProxyServer();
		proxy.start(8080);
		
		//Set the Desired Capabilities
		DesiredCapabilities caps = new DesiredCapabilities();
		caps.setCapability("deviceName", "Samsung");
		caps.setCapability("udid", "c87cd0a3"); //Give Device ID of your mobile phone
		caps.setCapability("platformName", "Android");
		caps.setCapability("platformVersion", "6.0.1");
		caps.setCapability("browserName", "Chrome");
		caps.setCapability("noReset", true);
		
		//Set ChromeDriver location
		System.setProperty("webdriver.chrome.driver", driverPath + "chromedriver.exe");
		
		//Instantiate Appium Driver
		//AppiumDriver<MobileElement> driver = null;
		try {
			driver = new RemoteWebDriver(new URL("http://127.0.0.1:4723/wd/hub"), caps);
			//driver = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), caps);
		} catch (MalformedURLException e) {
			System.out.println(e.getMessage());
		}
		wait = new WebDriverWait(driver, 60);
		
		driver.get(URL);
		System.out.println("Opening Initial Page");
		startTime();
		
		logJson = "\"StartTime\":\""+new Timestamp(System.currentTimeMillis())+"\"";
		seperator = ",";
		total=0;
		
		js = ((JavascriptExecutor) driver);
		
		Helper.noteTime("//*[@id='dashboard-container']/div/div[3]/div[1]/div[1]/a", "InitialPage");
		
		startTime();
		Helper.wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='dashboard-container']/div/div[3]/div[1]/div[1]/a"))).click();
	}

	public static void setUpSSO1() throws InterruptedException{
		DesiredCapabilities capabilities=DesiredCapabilities.android();
		// set the capability to execute test in chrome browser
		capabilities.setCapability(MobileCapabilityType.BROWSER_NAME,BrowserType.CHROME);

		// set the capability to execute our test in Android Platform
		capabilities.setCapability(MobileCapabilityType.PLATFORM,Platform.ANDROID);

		// we need to define platform name
		 capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME,"Android");

		// Set the device name as well (you can give any name)
		capabilities.setCapability(MobileCapabilityType.DEVICE_NAME,"c87cd0a3");
		//capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "8HYPUGDQ99999999");
		
		//Set the android version as well�
		capabilities.setCapability(MobileCapabilityType.VERSION,"6.0.1");
		
		capabilities.setCapability("unicodeKeyboard", true);
		capabilities.setCapability("resetKeyboard", true);

		// Create object of URL class and specify the appium server address
		URL url = null;
		try {
			url = new URL("http://127.0.0.1:4723/wd/hub");
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// Create object of  AndroidDriver class and pass the url and capability that we created
		driver = new RemoteWebDriver(url, capabilities);
		wait = new WebDriverWait(driver, 60);

		// start the proxyk
		//proxy = new BrowserMobProxyServer();
		//proxy.start(8080);

		// get the Selenium proxy object - org.openqa.selenium.Proxy;
		//Proxy seleniumProxy = ClientUtil.createSeleniumProxy(proxy);

		// configure it as a desired capability
		//DesiredCapabilities cap = new DesiredCapabilities();
		//cap.setCapability(CapabilityType.PROXY, seleniumProxy);

		// enable more detailed HAR capture, if desired (see CaptureType for the
		// complete list)
		//proxy.enableHarCaptureTypes(CaptureType.REQUEST_CONTENT, CaptureType.RESPONSE_CONTENT);

		// create a new HAR with the label "seleniumeasy.com"
		//proxy.newHar("Amazonpay");

		// open seleniumeasy.com
		js = ((JavascriptExecutor) driver);
		
		System.out.println("Opened Initial Payment Page");
		startTime();
		
		logJson = "\"StartTime\":\""+new Timestamp(System.currentTimeMillis())+"\"";
		seperator = ",";
		total=0;
		
		driver.get(URL);
		
		Helper.noteTime("//*[@id='dashboard-container']/div/div[3]/div[1]/div[1]/a", "InitialPage");
		startTime();
		
		Helper.wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='dashboard-container']/div/div[3]/div[1]/div[1]/a"))).click();
	}
	
	public static void setUpNonSSO() throws InterruptedException{
		//Start the Proxy
		proxy = new BrowserMobProxyServer();
		proxy.start(0);

		//Get the Selenium proxy object - org.openqa.selenium.Proxy;
		Proxy seleniumProxy = ClientUtil.createSeleniumProxy(proxy);
		
		//Set chrome driver system property
		System.setProperty("webdriver.chrome.driver", driverPath + "chromedriver.exe");
		
		//Add Chrome Options				
		ChromeOptions options = new ChromeOptions();
		//Chrome Option for add chrome profile to allow cache and cookies
		options.addArguments("--user-data-dir=C:\\Users\\sakshi20\\AppData\\Local\\Google\\Chrome\\User Data\\Default");
		//Maximized the screen
		options.addArguments("--start-maximized");

		//Configure it as a desired capability
		DesiredCapabilities capabilities = new DesiredCapabilities();				
		capabilities.setCapability(CapabilityType.PROXY, seleniumProxy);
		capabilities.setCapability(ChromeOptions.CAPABILITY, options);												        		
		
		// enable more detailed HAR capture, if desired (see CaptureType for the complete list)
		proxy.enableHarCaptureTypes(CaptureType.REQUEST_CONTENT, CaptureType.RESPONSE_CONTENT);		
			
		// create a new HAR with the label given here
		proxy.newHar("AmazonLogin");

		//Open chrome and enter url and go
		driver = new ChromeDriver(capabilities);
		driver.get(URL);
		
		wait = new WebDriverWait(driver, 60);
		js = ((JavascriptExecutor) driver);		
		System.out.println("Opened Initial Payment Page");
		startTime();

		logJson = "\"StartTime\":\""+new Timestamp(System.currentTimeMillis())+"\"";
		seperator = ",";
		total=0;
		
		Helper.wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='nav-xshop']/a[3]"))).click();
	}
	
	public static void setUpSSOPartialWallet() throws InterruptedException{
		System.out.println(driverPath + "chromedriver.exe");

		//set chromedriver system property
		System.setProperty("webdriver.chrome.driver", driverPath + "chromedriver.exe");
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--start-maximized");
		
		//start the proxy
		proxy = new BrowserMobProxyServer();
		proxy.start(0);		
		
		// get the Selenium proxy object - org.openqa.selenium.Proxy;
		seleniumProxy = ClientUtil.createSeleniumProxy(proxy);

		// configure it as a desired capability
		DesiredCapabilities capabilities = new DesiredCapabilities();	
		capabilities.setCapability(CapabilityType.PROXY, seleniumProxy);		
		capabilities.setCapability(ChromeOptions.CAPABILITY, options);			
		driver = new ChromeDriver(capabilities);

		// enable more detailed HAR capture, if desired (see CaptureType for the complete list
		proxy.enableHarCaptureTypes(CaptureType.REQUEST_CONTENT, CaptureType.RESPONSE_CONTENT);
		
		//create a new HAR with the label
		Helper.proxy.newHar("amazon.in");
		
		//Open URL
		driver.get(URL);
		wait = new WebDriverWait(driver, 60);
		js = ((JavascriptExecutor) driver);
		
		System.out.println("Opened Initial Payment Page");	
		
		startTime();
		System.out.println("Submit Clicked");
		System.out.println("Start Time  :  "+start);
		logJson = "\"StartTime\":\""+new Timestamp(System.currentTimeMillis())+"\"";
		seperator = ",";
		total=0;
		
		Helper.wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/form/input[4]"))).click();
	}
	
	public static void login()throws InterruptedException{
		System.out.println("Enter Amazon UserName and Password");
		wait.until(		
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='ap_email']")))
				.clear();
		wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='ap_email']")))
				.sendKeys(Creds.AMAZON_LOGIN_USERNAME);
		wait.until(		
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='ap_password']")))
				.clear();
		wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='ap_password']")))
				.sendKeys(Creds.AMAZON_LOGIN_PASSWORD);	
		
		//Get Payment option page time
		startTime();
		System.out.println("Login Submit Clicked");
		wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='signInSubmit']")))
				.click();
		System.out.println("Logged IN");
	}
	
	public static void startTime(){
		start = Instant.now();
//		System.out.println("Start Time  :  "+start);
//		logJson = "\"StartTime\":\""+new Timestamp(System.currentTimeMillis())+"\"";
//		seperator = ",";
//		total=0;
	}
	
	public static void noteTime(String element, String method)throws InterruptedException{
		if(element.length() != 0){
			wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(element)));
		}
		
		end = Instant.now();
		timeElapsed = Duration.between(start, end);
		total+=timeElapsed.toMillis();
		System.out.println("Time Taken in "+method+": "+ (double)timeElapsed.toMillis()/1000 +" Seconds");
		logJson+=seperator+"\""+method+"\":\""+timeElapsed.toMillis()+"\"";
		
		if(element.length() == 0){
			logJson+=seperator+"\"TotalTime\":\""+total+"\"";
			System.out.println("Total Time Taken: "+ total/1000 +" Seconds");
		}
	}
	
	public static void loginNonSso()throws InterruptedException{
		System.out.println("Opening Home Page");
		wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='nav-link-yourAccount']/span[1]")))
					.click();
		
		System.out.println("Enter Amazon UserName and Password");
		
		wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='ap_email']")))
				.sendKeys(Creds.AMAZON_LOGIN_USERNAME);
		
		wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='continue']")))
				.click();
		
		wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='ap_password']")))
				.sendKeys(Creds.AMAZON_LOGIN_PASSWORD);	
		
		wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='signInSubmit']")))
				.click();
		
		System.out.println("Logged IN");
	}
	
	
	public static void generateHarFile(String harFileName)throws InterruptedException{
		/*System.out.println("Generate HAR File");
		Har har = proxy.getHar();
		File harFile = new File(harFilePath+harFileName+"_"+System.currentTimeMillis()+".har");
		
		try {
			System.out.println("HARFILE : "+harFile);
			har.writeTo(harFile);
		} catch (IOException ex) {
			System.out.println(ex.toString());
			System.out.println("Could not find file " + harFile);
		}*/
		
		System.out.println("Generate Log File");
		generateLogFile(harFileName);
	}
	
	private static void generateLogFile(String logFileName){
		File logFile =  new File(logFilePath+logFileName+".txt");
		BufferedWriter bw = null;

	    try {
	         // APPEND MODE SET HERE
			 System.out.println("LOGFILE : "+logFile);
	         bw = new BufferedWriter(new FileWriter(logFile, true));
			 bw.write("{"+logJson+"}");
			 bw.newLine();
			 bw.flush();
	    } 
	    catch (IOException ioe) {
	    	ioe.printStackTrace();
	    } 
	    finally {// always close the file
			 if (bw != null) try {
			    bw.close();
			 } catch (IOException ioe2) {
			    // just ignore it
			 }
	    }
	}
	
	public static void waitForSuccess() throws InterruptedException{
		int count = 1;
		while(Helper.driver.getCurrentUrl().indexOf("https://www.amazon.in/gp/buy/thankyou") < 0){
			if(count > 2000){
				break;
			}
			count++;
		}
	}
	
	public static void endScript() throws InterruptedException{
		System.out.println("Closing Script\n*\n");

		if (driver != null) {
			proxy.stop();
			driver.quit();
		}
	}
	
	public static void endScript_() throws InterruptedException{
		System.out.println("Closing Script\n*\n*\n");

		if (driver != null) {
			proxy.stop();
			driver.quit();
		}
	}
	
	public static boolean isElementPresent(By by) {
	   try {
	      driver.findElement(by);
	      return true;
	   }
	   catch (NoSuchElementException e) {
	         return false;
	   }
    }
	

	public static Boolean verifyOTP() throws InterruptedException{
		String otp = "";
		while(otp.length()<6){
			System.out.println("OTP -> "+otp);
			Thread.sleep(500);
			//System.out.println(otp+":h:"+otp.length());
			if(driver.findElements(By.xpath("//*[@id='auth-pv-enter-code']")).size() > 0){
				otp = driver.findElement(By.xpath("//*[@id='auth-pv-enter-code']")).getAttribute("value");
			}else{
				System.out.println("nFound");
			}
		}
		return true;
	}
}