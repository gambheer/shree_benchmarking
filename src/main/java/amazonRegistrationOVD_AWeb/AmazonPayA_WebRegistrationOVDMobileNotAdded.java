package amazonRegistrationOVD_AWeb;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class AmazonPayA_WebRegistrationOVDMobileNotAdded {
	@BeforeTest
	public void setUp() throws InterruptedException {
		System.out.println("Running Amazon Pay Registration MobileNotAdded");
		Helper.setUpSSO();
	}
	
	@Test(priority = 1)
	public void RegistrationProcess() throws Exception{
		Helper.noteTime("//*[@id='ap_email_login']", "LoginPage");
		
		//Enter new email id for test flow
		Helper.wait.until(
				ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='ap_email_login']"))).sendKeys("moa07651@pqoss.com");
		
		Helper.startTime();
		Helper.driver.findElement(By.xpath("//*[@id='ap_login_form']")).submit();
		
		Helper.noteTime("//*[@id='ap_password']", "PasswordPage");
		
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='ap_password']"))).sendKeys("Abcd@123");
		
		Helper.startTime();
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='signInSubmit']"))).click();
	}
	
	@Test (priority=2)
	public void completeRegistration() throws Exception {
		Helper.noteTime("//*[@id='a-autoid-1']/span/input", "MobileAddPage");
		
		Helper.startTime();
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='a-autoid-1']/span/input"))).click();
		
		Helper.noteTime("//*[@id='ap_phone_number']", "MobileEnterPage");
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='ap_phone_number']"))).sendKeys("7073903383");
		
		Helper.startTime();
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='auth-continue']"))).click();
		
		Thread.sleep(3000);

		Helper.startTime();
		Helper.wait.until(
						ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='auth-verification-ok-announce']")))
								.click();
	
		Helper.wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='auth-pv-enter-code']")));
	
		Helper.verifyOTP();
	
		Helper.wait.until(
						ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='auth-verify-button']")))
								.click();
	
		Helper.noteTime("", "ThankYouPage");
	}
	
	@AfterTest
	public void tearDown() throws InterruptedException{
		String harFileName = this.getClass().getSimpleName();
		Helper.generateHarFile(harFileName);
		Helper.endScript();
	}
}