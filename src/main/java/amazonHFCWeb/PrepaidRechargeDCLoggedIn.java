package amazonHFCWeb;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import credsHelper.Creds;

public class PrepaidRechargeDCLoggedIn {
	@BeforeTest
	public void setUp() throws InterruptedException {
		System.out.println("Running Prepaid Recharge with Debit Card Logged In");
		Helper.startTime();
		Helper.setUpNonSSO();
	}
	
	@Test(priority = 1)
	public void RechargeProcess() throws Exception{
		Helper.noteTime("//*[@id='nav-xshop']/a[3]", "InitialPage");
		Helper.startTime();

		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='dashboard-container']/div/div[2]/div/div[4]/a")))
				.click();
		
		Helper.noteTime("//*[@id='mobileNumberTextInputId']", "RechargePage");
		Helper.startTime();

		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='mobileNumberTextInputId']")))
					.sendKeys(Creds.decrypt(Creds.RECHARGE_MOBILE_NUMBER));
		
		Thread.sleep(500);
		
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='amountTextInputId']")))
					.sendKeys(Creds.RECHARGE_AMOUNT);
		
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='buyButtonNative']")))
				.click();
		
		//If password asking again
		if(Helper.driver.getCurrentUrl().indexOf("https://www.amazon.in/ap/signin") >= 0){
			Helper.wait.until(
					ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='ap_password']")))
					.sendKeys(Creds.decrypt(Creds.AMAZON_LOGIN_PASSWORD));	
			
			Helper.wait.until(
					ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='signInSubmit']")))
					.click();
		}
	}
	
	@Test (priority=2)
	public void paymentSelection() throws Exception {
		Helper.noteTime("//*[@id='payment-change-link']", "PlaceOrderPage");
				
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='payment-change-link']"))).click();
		
		Helper.noteTime("//*[@id='new-vdc']/div[1]/span[1]/span/span/button", "PaymentPage");
		
		//Select Credit Card Option
		System.out.println("Select DC Option");
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='new-vdc']/div[1]/span[1]/span/span/button"))).click();
		
		Helper.wait.until(ExpectedConditions.elementToBeClickable(
				By.xpath("//*[@data-value='OtherBanks']"))).click();
		
		System.out.println("Enter Debit Card Name");
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='vdcName']")))
					.sendKeys(Creds.decrypt(Creds.ICICI_DEBIT_CARD_NAME));
		
		System.out.println("Enter Debit Card Number");					
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath(("//*[@id='newVerifiedDebitCardNumber']"))))
				.sendKeys(Creds.decrypt(Creds.ICICI_DEBIT_CARD));
		
		Thread.sleep(500);

		//select expiry month 
		Helper.wait.until(ExpectedConditions.elementToBeClickable(
				By.xpath("//*[@id='expirationInput']/div/span[1]/span/span/button"))).click();
		Helper.wait.until(ExpectedConditions.elementToBeClickable(
				By.xpath("//*[@data-value='"+Creds.ICICI_DEBIT_CARD_EXPIRY_MONTH_AMAZON_WEB+"']"))).click();
		
		//select expiry year
		Helper.wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='expirationInput']/div/span[2]/span/span/button"))).click();
		
		Helper.wait.until(ExpectedConditions.elementToBeClickable(
				By.xpath("//*[@data-value='"+Creds.ICICI_DEBIT_CARD_EXPIRY_YEAR_AMAZON_WEB+"']"))).click();
		
		Thread.sleep(1000);
		
		//Enter CVV
		System.out.println("Enter CVV number");
		Helper.wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='vdcCVVNum']"))).
																		sendKeys(Creds.decrypt(Creds.ICICI_DEBIT_CARD_CVV));
		//Click Add your card
		Helper.wait.until(ExpectedConditions.elementToBeClickable(By.name("addVDC"))).click();
		
		//Scroll down and click on continue btn
		Thread.sleep(5000);
		System.out.println("Scroll Down");
		Helper.js.executeScript("scroll(0, 1200);");
		Helper.wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='continue-bottom']"))).click();
		
		System.out.println("Click Place your order btn");
		Helper.wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='order-summary-box']/div[1]/div/div[1]/div/span/span/input"))).click();
	}
	
	@Test (priority=3)
	public void payment() throws Exception {
		System.out.println("Enter Debit Card Password");					
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='enterPASS']")))
				.sendKeys(Creds.decrypt(Creds.ICICI_DEBIT_CARD_PASSWORD));
		
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='sendotp']"))).click();
		
		Helper.waitForSuccess();
		Helper.noteTime("", "BankPayment");
	}	
	
	@AfterTest
	public void tearDown() throws InterruptedException{
		String harFileName = this.getClass().getSimpleName();
		Helper.generateHarFile(harFileName);
		Helper.endScript();
	}
}