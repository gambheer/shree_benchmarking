package amazonHFCWeb;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import credsHelper.Creds;

public class PrepaidRechargeCCLoggedIn {
	@BeforeTest
	public void setUp() throws InterruptedException {
		System.out.println("Running Prepaid Recharge with Credit Card Already LoggedIn");
		Helper.startTime();
		Helper.setUpNonSSO();
	}
	
	@Test(priority = 1)
	public void RechargeProcess() throws Exception{
		Helper.noteTime("//*[@id='nav-xshop']/a[3]", "InitialPage");
		Helper.startTime();

		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='dashboard-container']/div/div[2]/div/div[4]/a")))
				.click();
		
		Helper.noteTime("//*[@id='mobileNumberTextInputId']", "RechargePage");
		Helper.startTime();

		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='mobileNumberTextInputId']")))
					.sendKeys(Creds.decrypt(Creds.RECHARGE_MOBILE_NUMBER));
		
		Thread.sleep(500);
		
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='amountTextInputId']")))
					.sendKeys(Creds.RECHARGE_AMOUNT);
		
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='buyButtonNative']")))
				.click();
		
		//If password asking again
		if(Helper.driver.getCurrentUrl().indexOf("https://www.amazon.in/ap/signin") >= 0){
			Helper.wait.until(
					ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='ap_password']")))
					.sendKeys(Creds.decrypt(Creds.AMAZON_LOGIN_PASSWORD));	
			
			Helper.wait.until(
					ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='signInSubmit']")))
					.click();
		}
	}
	
	@Test (priority=2)
	public void paymentSelection() throws Exception {
		Helper.noteTime("//*[@id='payment-change-link']", "PlaceOrderPage");
				
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='payment-change-link']"))).click();
		
		Helper.noteTime("//*[@id='pm_new_verified_credit_card']", "PaymentPage");
		
		//Select Credit Card Option
		System.out.println("Select CC Option");
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='pm_new_verified_credit_card']"))).click();
		
		System.out.println("Enter Credit Card Name");
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='vccName']")))
					.sendKeys(Creds.decrypt(Creds.CREDIT_CARD_MASTER_NAME));
		
		System.out.println("Enter Credit Card Number");					
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath(("//*[@id='newVerifiedCreditCardNumber']"))))
				.sendKeys(Creds.decrypt(Creds.CREDIT_CARD_MASTER));
		
		Thread.sleep(200);
		//select expiry month 
		Helper.wait.until(ExpectedConditions.elementToBeClickable(
				By.xpath("//*[@id='new-vcc']/div[2]/div[2]/div[4]/span[1]/span/span/button"))).click();
		Helper.wait.until(ExpectedConditions.elementToBeClickable(
				By.xpath("//*[@data-value='"+Creds.CREDIT_CARD_MASTER_EXPIRY_MONTH_AMAZON_WEB+"']"))).click();
		
		Thread.sleep(200);
		//select expiry year
		Helper.wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='new-vcc']/div[2]/div[2]/div[4]/span[2]/span/span/button"))).click();
		Helper.wait.until(ExpectedConditions.elementToBeClickable(
				By.xpath("//*[@data-value='"+Creds.CREDIT_CARD_MASTER_EXPIRY_YEAR_AMAZON_WEB+"']"))).click();
		
		Thread.sleep(1000);
		//Enter CVV
		System.out.println("Enter CVV number");
		Helper.wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='new-vcc']/div[2]/div[2]/div[6]/input"))).
																		sendKeys(Creds.decrypt(Creds.CREDIT_CARD_MASTER_CVV));
		//Click Add your card
		Helper.wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='new-vcc']/div[2]/div[3]/div/span/span/input"))).click();
		
		//Scroll down and click on continue button
		while(Helper.driver.findElement(By.id("continue-bottom")).isDisplayed()){
			if(!Helper.driver.findElement(By.id("loading-spinner-blocker-doc")).isDisplayed()){
				Thread.sleep(500);
				Helper.js.executeScript("scroll(0, 2000);");
				break;
			}
		}
		Helper.wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='continue-bottom']"))).click();
		
		System.out.println("Click redirect to bank page");
		Helper.wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='redirect']"))).click();
		
		System.out.println("Click Place your order btn");
		Helper.wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='order-summary-box']/div[1]/div/div[1]/div/span/span/input"))).click();
	}
	
	@Test (priority=3)
	public void bankPayment() throws Exception {
		System.out.println("Enter Credit Card Password");					
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='txtPassword']")))
				.sendKeys(Creds.decrypt(Creds.CREDIT_CARD_MASTER_PASSWORD));
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='static']")))
				.click();
		
		Helper.waitForSuccess();
		Helper.noteTime("", "BankPayment");
	}
	
	@AfterTest
	public void tearDown() throws InterruptedException{
		String harFileName = this.getClass().getSimpleName();
		Helper.generateHarFile(harFileName);
		Helper.endScript();
	}
}