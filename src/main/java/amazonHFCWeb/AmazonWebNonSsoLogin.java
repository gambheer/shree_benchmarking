package amazonHFCWeb;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class AmazonWebNonSsoLogin {
	@BeforeTest
	public void setUp() throws InterruptedException {
		System.out.println("Running Amazon Web NonSSO Login");		
		Helper.setUpNonSSO();
	}

	@Test(priority = 1)
	public void login() throws InterruptedException{
		Helper.loginNonSso();
	}
	
	@AfterTest
	public void tearDown() throws InterruptedException{
		String harFileName = this.getClass().getSimpleName();
		Helper.generateHarFile(harFileName);
		Helper.endScript();
	}
}