package amazonHFCWeb;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import credsHelper.Creds;

public class PrepaidRechargeNBLoggedIn {
	@BeforeTest
	public void setUp() throws InterruptedException {
		System.out.println("Running Prepaid Recharge with Net Banking");
		Helper.startTime();
		Helper.setUpNonSSO();
	}

	@Test(priority = 1)
	public void RechargeProcess() throws Exception {
		Helper.noteTime("//*[@id='nav-xshop']/a[3]", "InitialPage");
		Helper.startTime();

		Helper.wait
				.until(ExpectedConditions
						.elementToBeClickable(By.xpath("//*[@id='dashboard-container']/div/div[2]/div/div[4]/a")))
				.click();

		Helper.noteTime("//*[@id='mobileNumberTextInputId']", "RechargePage");
		Helper.startTime();

		Helper.wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='mobileNumberTextInputId']")))
				.sendKeys(Creds.decrypt(Creds.RECHARGE_MOBILE_NUMBER));

		Thread.sleep(500);

		Helper.wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='amountTextInputId']")))
				.sendKeys(Creds.RECHARGE_AMOUNT);

		Helper.wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='buyButtonNative']"))).click();

		//If password asking again
		if(Helper.driver.getCurrentUrl().indexOf("https://www.amazon.in/ap/signin") >= 0){
			Helper.wait.until(
					ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='ap_password']")))
					.sendKeys(Creds.decrypt(Creds.AMAZON_LOGIN_PASSWORD));	
			
			Helper.wait.until(
					ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='signInSubmit']")))
					.click();
		}
	}

	@Test(priority = 2)
	public void paymentSelection() throws InterruptedException {
		Helper.noteTime("//*[@id='payment-change-link']", "PlaceOrderPage");

		Helper.wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='payment-change-link']"))).click();

		Helper.wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='pm_net_banking']"))).click();

		//Scroll down and click on continue btn
		System.out.println("Scroll Down");
		Helper.js.executeScript("scroll(0, 1200);");
				
		Helper.wait.until(ExpectedConditions
				.elementToBeClickable(By.xpath("//*[@id='net-banking']/div[2]/div[2]/div[1]/span/span/span/button")))
				.click();

		Helper.wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='1_dropdown_combobox']/li[5]/a"))).click();
		
		Helper.wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='continue-bottom']"))).click();

		System.out.println("Click Place your order btn");
		Helper.wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='order-summary-box']/div[1]/div/div[1]/div/span/span/input"))).click();
	}

	@Test(priority = 3)
	public void BankLogin() throws Exception {
		System.out.println("Enter SBI Bank credentials");

		Helper.wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='username']")))
				.sendKeys(Creds.decrypt(Creds.SBI_NETBANKING_LOGIN_ID));
		Helper.wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='label2']")))
				.sendKeys(Creds.decrypt(Creds.SBI_NETBANKING_PASSWORD));

		Helper.wait
				.until(ExpectedConditions
						.elementToBeClickable(By.xpath("//*[@id='login_block']/div[2]/div[1]/div[2]/div/input[1]"))).click();
	}

	@Test(priority = 4)
	public void payment() throws InterruptedException {
		System.out.println("Logged IN SBI Netbanking and Doing Payment Now");
		Helper.wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='Go']"))).click();

		System.out.println("Confirmed Payment");

		Helper.js.executeScript("window.scrollTo(0, document.body.scrollHeight)");

		if (!Helper.driver.findElement(By.xpath("//*[@id='confirmButton']")).isEnabled()) {
			System.out.println("Confirmed Payment Again");
			Helper.wait.until(ExpectedConditions.elementToBeClickable(By.id("aiplChkBox"))).click();
		}

		Helper.wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='confirmButton']"))).click();

		Helper.waitForSuccess();
		Helper.noteTime("", "BankPayment");
	}

	@AfterTest
	public void tearDown() throws InterruptedException {
		String harFileName = this.getClass().getSimpleName();
		Helper.generateHarFile(harFileName);
		Helper.endScript();
	}
}