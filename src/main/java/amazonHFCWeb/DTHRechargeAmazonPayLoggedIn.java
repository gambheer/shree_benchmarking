package amazonHFCWeb;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import credsHelper.Creds;

public class DTHRechargeAmazonPayLoggedIn {
	@BeforeTest
	public void setUp() throws InterruptedException {
		System.out.println("Running Prepaid Recharge with AmazonPay Wallet balance Already LoggedIn");
		Helper.startTime();
		Helper.setUpNonSSO();
	}
	
	@Test(priority = 1)
	public void RechargeProcess() throws Exception{
		Helper.noteTime("//*[@id='nav-xshop']/a[3]", "InitialPage");
		Helper.startTime();

		//Select DTH recharge
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='dashboard-container']/div/div[2]/div/div[5]/a")))
							.click();
		
		Helper.noteTime("//*[@id='nav-xshop']/a[3]", "DTHOPSelectionPage");
		Helper.startTime();
				
		//Select DTH operator
		Helper.wait.until(
				ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='a-page']/div[3]/div/div/div[2]/div[3]/a/div[1]/img")))
				.click();
		
		Helper.noteTime("//*[@id='nav-xshop']/a[3]", "DTHDetailPage");
		Helper.startTime();
		
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='auth_1']")))
					.sendKeys(Creds.decrypt(Creds.DTH_NUMBER));
		
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='dth-section']/div[3]/div[2]/div/div/input")))
					.sendKeys(Creds.DTH_RECHARGE_AMOUNT);
		
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='paymentBtnId']/span/input")))
				.click();
		
		//If password asking again
		if(Helper.driver.getCurrentUrl().indexOf("https://www.amazon.in/ap/signin") >= 0){
			Helper.wait.until(
					ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='ap_password']")))
					.sendKeys(Creds.decrypt(Creds.AMAZON_LOGIN_PASSWORD));	
			
			Helper.wait.until(
					ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='signInSubmit']")))
					.click();
		}
	}
	
	@Test (priority=2)
	public void payment() throws InterruptedException {
		Helper.noteTime("//*[@id='order-summary-box']/div[1]/div/div[1]/div/span/span/input", "PlaceOrderPage");
				
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='order-summary-box']/div[1]/div/div[1]/div/span/span/input")))
					.click();
		
		Helper.waitForSuccess();
		Helper.noteTime("", "ThankYouPage");		
	}
	
	@AfterTest
	public void tearDown() throws InterruptedException{
		String harFileName = this.getClass().getSimpleName();
		Helper.generateHarFile(harFileName);
		Helper.endScript();
	}
}