package paytmRegistrationOVDWeb;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import paytmRegistrationOVDWeb.Helper;
import credsHelper.Creds;

public class PaytmPayRegistrationOVDMobileVerified {
	@BeforeTest
	public void setUp() throws InterruptedException {
		System.out.println("Running Paytm Registration OVD");
		Helper.setUpSSO();
	}
	
	@Test(priority = 1)
	public void MobileVerified() throws Exception{
		Helper.noteTime("//*[@id='input_0']", "InitialPage");

		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='input_0']")))
					.sendKeys("8104048480");
		
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='input_1']")))
					.sendKeys("rohit#2412");
		//Click on Add Money
		Helper.startTime();
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='loginForm']/div/md-content/button[1]")))
							.click();
		
		Helper.verifyOTP();
		Helper.wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='verifyOTPForm']/div/div[2]/md-content/button[1]"))).click();
		
	}
}
