package amazonHFCApp;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import credsHelper.Creds;

public class PrepaidRechargeSCC {
	@BeforeTest
	public void setUp() throws InterruptedException {
		System.out.println("Running Prepaid Recharge with Saved Credit Card");
		Helper.startTime();
		Helper.setUpSSO();
	}
	
	@Test(priority = 1)
	public void RechargeProcess() throws Exception{
		Helper.noteTime("//*[@id='nav-xshop']/a[3]", "InitialPage");
		Helper.startTime();

		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='dashboard-container']/div/div[2]/div/div[4]/a")))
				.click();
		
		Helper.noteTime("//*[@id='mobileNumberTextInputId']", "RechargePage");
		Helper.startTime();

		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='mobileNumberTextInputId']")))
					.sendKeys(Creds.decrypt(Creds.RECHARGE_MOBILE_NUMBER));
		
		Thread.sleep(500);
		
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='amountTextInputId']")))
					.sendKeys(Creds.RECHARGE_AMOUNT);
		
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='buyButtonNative']")))
				.click();
		
		Helper.noteTime("//*[@id='ap_email']", "LoginPageUserName");
		Helper.startTime();

		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='ap_email']")))
					.sendKeys(Creds.decrypt(Creds.AMAZON_LOGIN_USERNAME));
		
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='continue']")))
					.click();
		
		Helper.noteTime("//*[@id='ap_email']", "LoginPagePassword");
		Helper.startTime();

		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='ap_password']")))
					.sendKeys(Creds.decrypt(Creds.AMAZON_LOGIN_PASSWORD));
				
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='signInSubmit']")))
					.click();
	}
	
	@Test (priority=2)
	public void paymentSelection() throws Exception {
		Helper.noteTime("//*[@id='payment-change-link']", "PlaceOrderPage");
				
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='payment-change-link']"))).click();
		
		Helper.noteTime("//*[@id='pm_new_verified_credit_card']", "PaymentPage");
		
		//Select Saved Credit Card Option
		System.out.println("Select CC Option");
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='pm_0']"))).click();
		
		//Enter CVV
		System.out.println("Enter CVV number");
		Helper.wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='existingCvvNum']"))).
																		sendKeys(Creds.decrypt(Creds.CREDIT_CARD_MASTER_CVV));
		
		Helper.wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='continue-top']"))).click();
		
		System.out.println("Click redirect to bank page");
		Helper.wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='redirect']"))).click();
		
		System.out.println("Click Place your order btn");
		Helper.wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='order-summary-box']/div[1]/div/div[1]/div/span/span/input"))).click();
	}
	
	@Test (priority=3)
	public void bankPayment() throws Exception {
		System.out.println("Enter Credit Card Password");					
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='txtPassword']")))
				.sendKeys(Creds.decrypt(Creds.CREDIT_CARD_MASTER_PASSWORD));
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='static']")))
				.click();
		
		Helper.waitForSuccess();
		Helper.noteTime("", "BankPayment");
	}
	
	@AfterTest
	public void tearDown() throws InterruptedException{
		String harFileName = this.getClass().getSimpleName();
		Helper.generateHarFile(harFileName);
		Helper.endScript();
	}
}