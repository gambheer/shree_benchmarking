package amazonRegistrationOVDWeb;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class AmazonPayRegistrationProvideOVD {
	@BeforeTest
	public void setUp() throws InterruptedException {
		System.out.println("Running Amazon Pay Registration OVD");
		Helper.setUpSSO();
	}
	
	@Test(priority = 1)
	public void RechargeProcess() throws Exception{
		Helper.noteTime("//*[@href='/gp/sva/addmoney']", "InitialPage");

		//Click on Add Money
		Helper.startTime();
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@href='/gp/sva/addmoney']")))
							.click();
		
		Helper.noteTime("//*[@id='ap_email']", "LoginPageUserName");
		
		Helper.startTime();
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='createAccountSubmit']")))
					.click();
		
		Helper.noteTime("//*[@id='ap_customer_name']", "RegistrationPage");

		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='ap_customer_name']")))
					.sendKeys("Rohit");
		
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='ap_phone_number']")))
					.sendKeys("81040484800");
		
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='ap_email']")))
					.sendKeys("k164708@nwytg.com");
		
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='ap_password']")))
					.sendKeys("Abcd@1234");
		
		Helper.startTime();
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='continue']")))
					.click();
		
		Helper.noteTime("//*[@id='auth-verify-button']", "MobileVerificationPage");
		
//		Helper.startTime();
		Helper.verifyOTP();
//		Helper.noteTime("//*[@id='auth-verify-button']", "Filling OTP By User");
		
		Helper.startTime();
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='auth-verify-button']")))
					.click();
	}
	
	@Test (priority=2)
	public void completeRegistration() throws Exception {
		Helper.js.executeScript("scroll(0, 1200);");
		
		Helper.noteTime("//*[@id='ovdValue']", "OVDPage");
		
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='a-autoid-0']/span/input")))
					.click();
		
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='sva-customer-name-input']"))).sendKeys("Gambs");
		
		Helper.startTime();
		
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='insva-edit-customer-name-button']/span/input")))
					.click();
		
		Helper.noteTime("//*[@id='sva-customer-name-input']", "EditNamePage");
				
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='ow-noAadhaar']/span[2]/a"))).click();
		
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='a-autoid-2-announce']/span")))
					.click();
		
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='ovdTypeDropdown_1']")))
					.click();
		
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='ovdValue']"))).sendKeys("123456789");
		
		Helper.noteTime("", "ThankYouPage");		
	}
	
	@AfterTest
	public void tearDown() throws InterruptedException{
		String harFileName = this.getClass().getSimpleName();
		Helper.generateHarFile(harFileName);
		Helper.endScript();
	}
}
