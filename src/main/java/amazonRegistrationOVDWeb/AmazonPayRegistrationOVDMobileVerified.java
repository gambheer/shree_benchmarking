package amazonRegistrationOVDWeb;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import credsHelper.Creds;

public class AmazonPayRegistrationOVDMobileVerified {
	@BeforeTest
	public void setUp() throws InterruptedException {
		System.out.println("Running Amazon Pay Registration OVD");
		Helper.setUpSSO();
	}
	
	@Test(priority = 1)
	public void RechargeProcess() throws Exception{
		Helper.noteTime("//*[@href='/gp/sva/addmoney']", "InitialPage");

		//Click on Add Money
		Helper.startTime();
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@href='/gp/sva/addmoney']")))
							.click();
		
		Helper.noteTime("//*[@id='ap_email']", "LoginPageUserName");

		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='ap_email']")))
					.sendKeys(Creds.decrypt(Creds.AMAZON_LOGIN_USERNAME));
		
		Helper.startTime();
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='continue']")))
					.click();
		
		Helper.noteTime("//*[@id='ap_email']", "LoginPagePassword");

		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='ap_password']")))
					.sendKeys(Creds.decrypt(Creds.AMAZON_LOGIN_PASSWORD));
		
		Helper.startTime();
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='signInSubmit']")))
					.click();
	}
	
	@Test (priority=2)
	public void completeRegistration() throws Exception {
		Helper.js.executeScript("scroll(0, 1200);");
		
		Helper.noteTime("//*[@id='ovdValue']", "OVDPage");
		
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='a-autoid-0']/span/input")))
					.click();
		
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='sva-customer-name-input']"))).sendKeys("Gambs");
		
		Helper.startTime();
		
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='insva-edit-customer-name-button']/span/input")))
					.click();
		
		Helper.noteTime("//*[@id='sva-customer-name-input']", "EditNamePage");
				
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='ovdValue']"))).sendKeys(Creds.decrypt(Creds.AADHAAR_NUMBER));
		
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='insva-registration-button']/span/input")))
					.click();
		
		Helper.noteTime("", "ThankYouPage");		
	}
	
	@AfterTest
	public void tearDown() throws InterruptedException{
		String harFileName = this.getClass().getSimpleName();
		Helper.generateHarFile(harFileName);
		Helper.endScript();
	}
}