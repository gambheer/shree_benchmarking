package amazonRegistrationOVDWeb;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import amazonRegistrationOVDWeb.Helper;

public class AmazonPayRegistrationOVDMobileNotAdded {
	@BeforeTest
	public void setUp() throws InterruptedException {
		System.out.println("Running Amazon Pay Registration OVD");
		Helper.setUpSSO();
	}
	
	@Test(priority = 1)
	public void MobileNotAdded() throws Exception{
		Helper.noteTime("//*[@href='/gp/sva/addmoney']", "InitialPage");

		//Click on Add Money
		Helper.startTime();
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@href='/gp/sva/addmoney']")))
							.click();
		
		Helper.noteTime("//*[@id='ap_email']", "LoginPageUserName");

		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='ap_email']")))
					.sendKeys("k150412@nwytg.com");
		
		Helper.startTime();
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='continue']")))
					.click();
		
		Helper.noteTime("//*[@id='ap_email']", "LoginPagePassword");

		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='ap_password']")))
					.sendKeys("Abcd@1234");
		
		Helper.startTime();
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='signInSubmit']")))
					.click();
		
		Helper.noteTime("//*[@id='a-autoid-1']/span/input", "AddMobilePage");
		
		Helper.startTime();
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='a-autoid-1']/span/input")))
					.click();
		
		Helper.noteTime("//*[@id='ap_phone_number']", "AddMobileForm");
		
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='ap_phone_number']")))
					.sendKeys("70146295962");
		
		
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='auth-continue']")))
					.click();
		
		Thread.sleep(3000);
//		Helper.wait.until(
//				ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//*[@id='a-popover-header-1']")));
		Helper.startTime();
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='auth-verification-ok-announce']")))
					.click();
		
//		Helper.noteTime("//*[@id='auth-verification-ok-announce']", "OTPPopUp");
		
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='auth-pv-enter-code']")));
		
		Helper.verifyOTP();
		
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='auth-verify-button']")))
					.click();
		
		Helper.noteTime("", "ThankYouPage");
	}
	
	@AfterTest
	public void tearDown() throws InterruptedException{
		String harFileName = this.getClass().getSimpleName();
		Helper.generateHarFile(harFileName);
		Helper.endScript();
	}
}
