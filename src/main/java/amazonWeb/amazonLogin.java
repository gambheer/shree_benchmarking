package amazonWeb;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class amazonLogin {
	@BeforeTest
	public void setUp() throws InterruptedException {
		System.out.println("Running Amazon Web Login");		
		Helper.setUpNonSSO();
	}

	@Test(priority = 1)
	public void login() throws InterruptedException{
		Helper.noteTime("//*[@id='ap_email']", "Login");
		Helper.loginNonSso();
		Helper.noteTime("", "Login");
	}
	
	@AfterTest
	public void tearDown() throws InterruptedException{
		String harFileName = this.getClass().getSimpleName();
		Helper.generateHarFile(harFileName);
		Helper.endScript();
	}
}
