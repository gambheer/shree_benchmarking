package amazonWeb;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import credsHelper.*;

public class AmazonWebCCMaster {
	@BeforeTest
	public void setUp() throws InterruptedException {
		System.out.println("Running Amazon Web Login");		
		Helper.startTime();
		Helper.setUpNonSSO();
	}

	@Test(priority = 1)
	public void amazonPayWallet() throws InterruptedException{
		Helper.noteTime("//*[@id='nav-xshop']/a[3]", "InitialPage");
		Helper.wait.until(		
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='nav-xshop']/a[3]")))
				.click();
		
		//If password asking again
		if(Helper.driver.getCurrentUrl().indexOf("https://www.amazon.in/ap/signin") >= 0){
			Helper.wait.until(
					ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='ap_password']")))
					.sendKeys(Creds.AMAZON_LOGIN_PASSWORD);	
			
			Helper.wait.until(
					ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='signInSubmit']")))
					.click();
		}
		
		Helper.noteTime("//*[@id='dashboard-container']/div/div[2]/div/div[1]", "AmazonPayWalletPage");
		Helper.wait.until(		
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='dashboard-container']/div/div[2]/div/div[1]")))
				.click();
		
		//If Password Asking Again
		if(Helper.driver.getCurrentUrl().indexOf("https://www.amazon.in/ap/signin") >= 0){
			Helper.wait.until(
					ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='ap_password']")))
					.sendKeys(Creds.AMAZON_LOGIN_PASSWORD);	
			
			Helper.wait.until(
					ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='signInSubmit']")))
					.click();
		}
		
		Helper.noteTime("//*[@id='sva-asv-manual-reload-amount']", "LoadMoneyPage");
		Helper.wait.until(		
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='sva-asv-manual-reload-amount']")))
				.clear();
		Helper.wait.until(		
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='sva-asv-manual-reload-amount']")))
				.sendKeys("1");
		
		Helper.js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		
		Helper.wait.until(		
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='pm_0']")))
				.click();
		Helper.wait.until(		
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='existingCvvNum']")))
				.sendKeys(Creds.CREDIT_CARD_MASTER_CVV);
		
		Helper.wait.until(		
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='a-autoid-4']/span/input")))
				.click();
	}
	
	@Test (priority=2)
	public void payment() throws InterruptedException {		
		Helper.noteTime("//*[@id='txtPassword']", "PaymentPage");
						
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='txtPassword']")))
				.sendKeys(Creds.CREDIT_CARD_MASTER_PASSWORD);
		Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='static']")))
				.click();
		
		Helper.waitForSuccess();
		Helper.noteTime("", "BankPayment");
	}
	
	@AfterTest
	public void tearDown() throws InterruptedException{
		String harFileName = this.getClass().getSimpleName();
		Helper.generateHarFile(harFileName);
		Helper.endScript();
	}
}
