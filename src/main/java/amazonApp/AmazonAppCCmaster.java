package amazonApp;

import java.net.MalformedURLException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import io.appium.java_client.TouchAction;
import java.util.List;
import credsHelper.Creds;

public class AmazonAppCCmaster{
  @BeforeTest
  public void setUp() throws InterruptedException, MalformedURLException {
		System.out.println("Running Andorid App credit card master");		
		Helper.setUP();
  }

  @Test(priority = 1)
  public void login() throws InterruptedException{
		Helper.login();
  }
  
  @Test(priority=2)
  public void bankDetail() throws InterruptedException { 
		System.out.println("On Checkout Page");
		Helper.wait.until(
					ExpectedConditions.presenceOfAllElementsLocatedBy(
							By.name("Amazon Payments Checkout"))).equals("complete"); 

  		System.out.println("Select Credit Card option");
	    WebElement ele2 = Helper.driver.findElement(By.name("Amazon Payments Checkout"));
	    //select credit card
	    List<WebElement> child = ele2.findElements(By.name("Credit card"));
	    child.get(0).click();
  		System.out.println("CC Selected");
	    
  		//Enter credit card details
	    Helper.wait.until(
					ExpectedConditions.elementToBeClickable(
							By.className("android.widget.EditText"))).sendKeys(Creds.CREDIT_CARD_MASTER);
	    Helper.driver.hideKeyboard();
	    
	    Helper.wait.until(
					ExpectedConditions.elementToBeClickable(
							By.name("Name on card"))).sendKeys(Creds.CREDIT_CARD_MASTER_NAME);
	    Helper.driver.hideKeyboard();
	    
	    //Select Month by scroll if needed
	    Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.name("Expiration Month"))).click();
	    if(Integer.parseInt(Creds.CREDIT_CARD_MASTER_EXPIRY_MONTH_AMAZON_APP) > 7){
		    WebElement fromElement= Helper.driver.findElement(By.name("1"));
		    WebElement toElement= Helper.driver.findElement(By.name(Creds.CREDIT_CARD_MASTER_EXPIRY_MONTH_AMAZON_APP));
		    TouchAction tAction=new TouchAction(Helper.driver);
		    tAction.press(fromElement).moveTo(toElement).perform();
		    Thread.sleep(1000);
		    tAction.tap(500,500).perform();
	    }
	    Helper.wait.until(
				ExpectedConditions.elementToBeClickable(
						By.name(Creds.CREDIT_CARD_MASTER_EXPIRY_MONTH_AMAZON_APP))).click();
	    
	    //Select Year by scroll
	    Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.name("Expiration Year"))).click();
	    if(Integer.parseInt(Creds.CREDIT_CARD_MASTER_EXPIRY_YEAR_AMAZON_APP) > 2023){
		    WebElement fromElement= Helper.driver.findElement(By.name("2017"));
		    WebElement toElement= Helper.driver.findElement(By.name(Creds.CREDIT_CARD_MASTER_EXPIRY_YEAR_AMAZON_APP));
		    TouchAction tAction=new TouchAction(Helper.driver);
		    tAction.press(fromElement).moveTo(toElement).perform();
		    Thread.sleep(1000);
		    tAction.tap(500,500).perform();
	    }
	    Helper.wait.until(
					ExpectedConditions.elementToBeClickable(
							By.name(Creds.CREDIT_CARD_MASTER_EXPIRY_YEAR_AMAZON_APP))).click();
	    
	    //Enter CVV
	    Helper.wait.until(
					ExpectedConditions.elementToBeClickable(By.name("Enter CVV"))).
											sendKeys(Creds.CREDIT_CARD_MASTER_CVV);
	    Helper.driver.hideKeyboard();
	    
	    //Press Add Your Card Btn
	    WebElement addYourCard = Helper.driver.findElement(By.name("Add your card"));
	    TouchAction tapAction=new TouchAction(Helper.driver);
	    tapAction.tap(addYourCard).tap(addYourCard).perform();

	    //Enter CVV Again
	    Helper.wait.until(
							ExpectedConditions.elementToBeClickable(By.name("Enter CVV"))).
	    											sendKeys(Creds.CREDIT_CARD_MASTER_CVV);
		Helper.driver.hideKeyboard();
	    
		//Click Pay Now
		Helper.driver.findElement(By.name("Pay Now Pay Now")).click();
  	}

  	@Test(priority=3)
	public void payment() throws InterruptedException {
	  //enter password for axis bank credit card
	  Helper.wait.until(
				ExpectedConditions.presenceOfElementLocated(By.name("Password"))).sendKeys(Creds.CREDIT_CARD_VISA_PASSWORD);
	  Helper.driver.hideKeyboard();
	  
	  //click on submit button to confirm payment
	  Helper.wait.until(
				ExpectedConditions.elementToBeClickable(By.name("SUBMIT"))).click();
	  
	  Helper.waitForSuccess();
  	}
  	
  	@AfterTest
	public void tearDown() throws InterruptedException{
		String harFileName = this.getClass().getSimpleName();
		Helper.generateHarFile(harFileName);
		Helper.endScript();
	}
}