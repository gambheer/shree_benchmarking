package amazonApp;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;
import java.time.Instant;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import net.lightbody.bmp.BrowserMobProxy;
import net.lightbody.bmp.core.har.Har;
import credsHelper.Creds;

public class Helper{
	public static JavascriptExecutor jse;
	public static WebDriverWait wait;
	public static AndroidDriver<MobileElement> driver;
	public static BrowserMobProxy proxy;
	public static ExpectedConditions expectedCondition;
	
	public static String driverPath = System.getProperty("user.dir") + "\\resources\\"; 
	public static String harFilePath = System.getProperty("user.dir") + "\\resources\\harfiles\\";
	public static String logFilePath = System.getProperty("user.dir") + "\\resources\\logfiles\\";
	
	public static Instant start ;
	public static Instant end ;
	public static Duration timeElapsed;
	
	public static int total=0;
	
	public static String logJson = "";
	public static String seperator = "";
	
	static Proxy seleniumProxy;

	public static void setUP() throws InterruptedException, MalformedURLException{
		  new DesiredCapabilities();
		  DesiredCapabilities cap = DesiredCapabilities.android();
	      
		  cap.setCapability("deviceName", "Samsung");
	      cap.setCapability("udid", "c87cd0a3");
		  cap.setCapability("platformVersion", "6.0.1");
	      cap.setCapability("platformName", "Android");
	      cap.setCapability("appPackage", "in.amazon.mShop.android.shopping");
	      cap.setCapability("appActivity", "com.amazon.mShop.splashscreen.StartupActivity");

//	      proxy = new BrowserMobProxyServer();
//	      proxy.start(0);
//	      
//	      // enable more detailed HAR capture, if desired (see CaptureType for the complete list)
//	      proxy.enableHarCaptureTypes(CaptureType.REQUEST_CONTENT, CaptureType.RESPONSE_CONTENT);

	      // create a new HAR with the label "seleniumeasy.com"
////	      proxy.newHar("Amazon App");
//	        
//	      // get the Selenium proxy object - org.openqa.selenium.Proxy;
//		  Proxy seleniumProxy = ClientUtil.createSeleniumProxy(proxy);

		  // configure it as a desired capability
		  DesiredCapabilities caps = new DesiredCapabilities();
//		  caps.setCapability(CapabilityType.PROXY, seleniumProxy);
	      
	      driver = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), cap);

	      wait = new WebDriverWait(driver,40);
	      jse = (JavascriptExecutor)driver;	
	}
	
	public static void login()throws InterruptedException{
	      //Uncomment if want to login again
		  System.out.println("Login to Amazon");
	      wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("Amazon Sign In")));
	      
	      System.out.println("open login page");	      
	      WebElement ele1 = driver.findElement(By.name("Amazon Sign In"));
	      
	      System.out.println("Element"+ele1);
	      
	      if(ele1.isDisplayed()){
		      System.out.println(ele1.isDisplayed());
		      System.out.println("Find element");
	
		      List <WebElement> child1 = ele1.findElements(By.className("android.widget.EditText"));
		      child1.get(0).sendKeys(Creds.AMAZON_LOGIN_USERNAME);
		      //driver.hideKeyboard();
		      child1.get(1).sendKeys(Creds.AMAZON_LOGIN_PASSWORD);
		      driver.hideKeyboard();
		      
		      wait.until(ExpectedConditions.elementToBeClickable(By.name("Login"))).click();
	      }
	}
	
	public static void generateHarFile(String harFileName)throws InterruptedException{
		System.out.println("Generate HAR File");
		Har har = proxy.getHar();
		File harFile = new File(harFilePath+harFileName+"_"+System.currentTimeMillis()+".har");
		
		try {
			System.out.println("HARFILE : "+harFile);
			har.writeTo(harFile);	
		} catch (IOException ex) {
			System.out.println(ex.toString());
			System.out.println("Could not find file " + harFile);
		}
	}
	
	public static void waitForSuccess() throws InterruptedException{
		Thread.sleep(10000);
	}
	
	public static void endScript() throws InterruptedException{
		System.out.println("Closing Script\n*\n*\n");

		if (driver != null) {
			proxy.stop();
			driver.closeApp();
		}
	}
}